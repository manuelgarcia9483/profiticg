﻿Public Class Clase1
    Public XServidorP As String
    Public XUsuarioP As String
    Public XPasswordP As String
    Public XbdP As String
    Public XServidorHD As String
    Public XUsuarioHD As String
    Public XPasswordHD As String
    Public XbdHD As String
    Public Xusuario As String
    Public Xsucursal As String
    Public XAlmacen As String
    Public XMoneda As String
    Public XTipoa As String
    Public XCtrans As String
    Public XFpago As String
    Public XTipoa2 As String
    Public XNVista As String
    Public CnnString As String
    Dim f_HeightRatio As Single = New Single
    Dim f_WidthRatio As Single = New Single
    Public Sub ResizeForm(ByVal ObjForm As Form, ByVal DesignerWidth As Integer, ByVal DesignerHeight As Integer)
        Dim i_StandardHeight As Integer = DesignerHeight
        Dim i_StandardWidth As Integer = DesignerWidth
        Dim i_PresentHeight As Integer = Screen.PrimaryScreen.Bounds.Height
        Dim i_PresentWidth As Integer = Screen.PrimaryScreen.Bounds.Width
        f_HeightRatio = (CSng(i_PresentHeight) / CSng(i_StandardHeight))
        f_WidthRatio = (CSng(i_PresentWidth) / CSng(i_StandardWidth))
        ObjForm.AutoScaleMode = AutoScaleMode.None
        ObjForm.Scale(New SizeF(f_WidthRatio, f_HeightRatio))
        For Each c As Control In ObjForm.Controls
            If c.HasChildren Then
                ResizeControlStore(c)
            Else
                c.Font = New Font(c.Font.FontFamily, c.Font.Size * f_HeightRatio, c.Font.Style, c.Font.Unit, (CByte(0)))
            End If
        Next
        ObjForm.Font = New Font(ObjForm.Font.FontFamily, ObjForm.Font.Size * f_HeightRatio, ObjForm.Font.Style, ObjForm.Font.Unit, (CByte(0)))
    End Sub
    Private Sub ResizeControlStore(ByVal objCtl As Control)
        If objCtl.HasChildren Then
            For Each cChildren As Control In objCtl.Controls
                If cChildren.HasChildren Then
                    ResizeControlStore(cChildren)
                Else
                    cChildren.Font = New Font(cChildren.Font.FontFamily, cChildren.Font.Size * f_HeightRatio, cChildren.Font.Style, cChildren.Font.Unit, (CByte(0)))
                End If
            Next
            objCtl.Font = New Font(objCtl.Font.FontFamily, objCtl.Font.Size * f_HeightRatio, objCtl.Font.Style, objCtl.Font.Unit, (CByte(0)))
        Else
            objCtl.Font = New Font(objCtl.Font.FontFamily, objCtl.Font.Size * f_HeightRatio, objCtl.Font.Style, objCtl.Font.Unit, (CByte(0)))
        End If
    End Sub

    Public Sub Abrir_ini()
        Dim a1 As Integer
        Dim Linea As String

        FileOpen(1, My.Application.Info.DirectoryPath & "\config.ini", OpenMode.Input)
        Linea = LineInput(1)
        a1 = InStr(1, Linea, ":") + 1
        XServidorP = IIf(a1 > 0, Trim(Mid(Linea, a1, Len(Linea))), "")
        Linea = LineInput(1)
        a1 = InStr(1, Linea, ":") + 1
        XUsuarioP = IIf(a1 > 0, Trim(Mid(Linea, a1, Len(Linea))), "")
        Linea = LineInput(1)
        a1 = InStr(1, Linea, ":") + 1
        XPasswordP = IIf(a1 > 0, Trim(Mid(Linea, a1, Len(Linea))), "")
        Linea = LineInput(1)
        a1 = InStr(1, Linea, ":") + 1
        XbdP = IIf(a1 > 0, Trim(Mid(Linea, a1, Len(Linea))), "")
        Linea = LineInput(1)
        a1 = InStr(1, Linea, ":") + 1
        XServidorHD = IIf(a1 > 0, Trim(Mid(Linea, a1, Len(Linea))), "")
        Linea = LineInput(1)
        a1 = InStr(1, Linea, ":") + 1
        XUsuarioHD = IIf(a1 > 0, Trim(Mid(Linea, a1, Len(Linea))), "")
        Linea = LineInput(1)
        a1 = InStr(1, Linea, ":") + 1
        XPasswordHD = IIf(a1 > 0, Trim(Mid(Linea, a1, Len(Linea))), "")
        Linea = LineInput(1)
        a1 = InStr(1, Linea, ":") + 1
        XbdHD = IIf(a1 > 0, Trim(Mid(Linea, a1, Len(Linea))), "")
        Linea = LineInput(1)
        a1 = InStr(1, Linea, ":") + 1
        XNVista = IIf(a1 > 0, Trim(Mid(Linea, a1, Len(Linea))), "")
        Linea = LineInput(1)
        a1 = InStr(1, Linea, ":") + 1
        Xsucursal = IIf(a1 > 0, Trim(Mid(Linea, a1, Len(Linea))), "")
        Linea = LineInput(1)
        a1 = InStr(1, Linea, ":") + 1
        XAlmacen = IIf(a1 > 0, Trim(Mid(Linea, a1, Len(Linea))), "")
        Linea = LineInput(1)
        a1 = InStr(1, Linea, ":") + 1
        Xusuario = IIf(a1 > 0, Trim(Mid(Linea, a1, Len(Linea))), "")
        Linea = LineInput(1)
        a1 = InStr(1, Linea, ":") + 1
        XMoneda = IIf(a1 > 0, Trim(Mid(Linea, a1, Len(Linea))), "")
        Linea = LineInput(1)
        a1 = InStr(1, Linea, ":") + 1
        XTipoa = IIf(a1 > 0, Trim(Mid(Linea, a1, Len(Linea))), "")
        Linea = LineInput(1)
        a1 = InStr(1, Linea, ":") + 1
        XCtrans = IIf(a1 > 0, Trim(Mid(Linea, a1, Len(Linea))), "")
        Linea = LineInput(1)
        a1 = InStr(1, Linea, ":") + 1
        XFpago = IIf(a1 > 0, Trim(Mid(Linea, a1, Len(Linea))), "")
        Linea = LineInput(1)
        a1 = InStr(1, Linea, ":") + 1
        XTipoa2 = IIf(a1 > 0, Trim(Mid(Linea, a1, Len(Linea))), "")
        FileClose(1)
        CnnString = ("Provider=SQLOLEDB;Data Source=" & XServidorP & ";user id=" & XUsuarioP & "; password=" & XPasswordP & "; Initial Catalog=" & XbdP)
    End Sub


End Class
