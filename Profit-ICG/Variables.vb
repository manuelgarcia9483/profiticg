﻿'Imports System.Data.SQLite
Module Variables
    Public XServidorP As String
    Public XUsuarioP As String
    Public XPasswordP As String
    Public XbdP As String
    '    Public XServidorHD As String
    '    Public XUsuarioHD As String
    '    Public XPasswordHD As String
    '    Public XbdHD As String
    Public Xusuario As String
    Public Xsucursal As String
    Public XAlmacen As String
    Public XMoneda As String
    Public XTipoa As String
    Public XCtrans As String
    Public XFpago As String
    Public XTipoa2 As String
    Public XNVista As String
    Public XFormaInterface As Integer
    Public XArchivo As String = ""
    Public XCta_ingreso As String
    Public CnnString As String
    Public CnnStringA As String
    Public Campoab1 As String = ""
    Public Campoac1 As String = ""
    Public Campoav1 As String = ""
    Public XTipoV As String = ""
    Public XClienteO As String = ""
    Dim f_HeightRatio As Single = New Single
    Dim f_WidthRatio As Single = New Single
    Public Sub ResizeForm(ByVal ObjForm As Form, ByVal DesignerWidth As Integer, ByVal DesignerHeight As Integer)
        Dim i_StandardHeight As Integer = DesignerHeight
        Dim i_StandardWidth As Integer = DesignerWidth
        Dim i_PresentHeight As Integer = Screen.PrimaryScreen.Bounds.Height
        Dim i_PresentWidth As Integer = Screen.PrimaryScreen.Bounds.Width
        f_HeightRatio = (CSng(i_PresentHeight) / CSng(i_StandardHeight))
        f_WidthRatio = (CSng(i_PresentWidth) / CSng(i_StandardWidth))
        ObjForm.AutoScaleMode = AutoScaleMode.None
        ObjForm.Scale(New SizeF(f_WidthRatio, f_HeightRatio))
        For Each c As Control In ObjForm.Controls
            If c.HasChildren Then
                ResizeControlStore(c)
            Else
                c.Font = New Font(c.Font.FontFamily, c.Font.Size * f_HeightRatio, c.Font.Style, c.Font.Unit, (CByte(0)))
            End If
        Next
        ObjForm.Font = New Font(ObjForm.Font.FontFamily, ObjForm.Font.Size * f_HeightRatio, ObjForm.Font.Style, ObjForm.Font.Unit, (CByte(0)))
    End Sub
    Private Sub ResizeControlStore(ByVal objCtl As Control)
        If objCtl.HasChildren Then
            For Each cChildren As Control In objCtl.Controls
                If cChildren.HasChildren Then
                    ResizeControlStore(cChildren)
                Else
                    cChildren.Font = New Font(cChildren.Font.FontFamily, cChildren.Font.Size * f_HeightRatio, cChildren.Font.Style, cChildren.Font.Unit, (CByte(0)))
                End If
            Next
            objCtl.Font = New Font(objCtl.Font.FontFamily, objCtl.Font.Size * f_HeightRatio, objCtl.Font.Style, objCtl.Font.Unit, (CByte(0)))
        Else
            objCtl.Font = New Font(objCtl.Font.FontFamily, objCtl.Font.Size * f_HeightRatio, objCtl.Font.Style, objCtl.Font.Unit, (CByte(0)))
        End If
    End Sub
    '    Public Function Buscar_BD(ByVal rif As String, ByVal sucursal As String)
    'Dim Xbdp As String = ""
    '
    '   Return XbdP
    'End Function

    Public Sub Abrir_ini(ByVal xempresa As String)
        Dim a1 As Integer
        Dim Linea As String

        FileOpen(10, My.Application.Info.DirectoryPath & "\config.ini", OpenMode.Input)
        Linea = LineInput(10)
        a1 = InStr(1, Linea, ":") + 1
        XServidorP = IIf(a1 > 0, Trim(Mid(Linea, a1, Len(Linea))), "")
        Linea = LineInput(10)
        a1 = InStr(1, Linea, ":") + 1
        XUsuarioP = IIf(a1 > 0, Trim(Mid(Linea, a1, Len(Linea))), "")
        Linea = LineInput(10)
        a1 = InStr(1, Linea, ":") + 1
        XPasswordP = IIf(a1 > 0, Trim(Mid(Linea, a1, Len(Linea))), "")
        Linea = LineInput(10)
        a1 = InStr(1, Linea, ":") + 1
        XbdP = IIf(a1 > 0, Trim(Mid(Linea, a1, Len(Linea))), "")
        Linea = LineInput(10)
        a1 = InStr(1, Linea, ":") + 1
        Xsucursal = IIf(a1 > 0, Trim(Mid(Linea, a1, Len(Linea))), "")
        Linea = LineInput(10)
        a1 = InStr(1, Linea, ":") + 1
        XAlmacen = IIf(a1 > 0, Trim(Mid(Linea, a1, Len(Linea))), "")
        Linea = LineInput(10)
        a1 = InStr(1, Linea, ":") + 1
        Xusuario = IIf(a1 > 0, Trim(Mid(Linea, a1, Len(Linea))), "")
        Linea = LineInput(10)
        a1 = InStr(1, Linea, ":") + 1
        XMoneda = IIf(a1 > 0, Trim(Mid(Linea, a1, Len(Linea))), "")
        Linea = LineInput(10)
        a1 = InStr(1, Linea, ":") + 1
        XCtrans = IIf(a1 > 0, Trim(Mid(Linea, a1, Len(Linea))), "")
        Linea = LineInput(10)
        a1 = InStr(1, Linea, ":") + 1
        XFpago = IIf(a1 > 0, Trim(Mid(Linea, a1, Len(Linea))), "")
        Linea = LineInput(10)
        a1 = InStr(1, Linea, ":") + 1
        XCta_ingreso = IIf(a1 > 0, Trim(Mid(Linea, a1, Len(Linea))), "")
        Linea = LineInput(10)
        a1 = InStr(1, Linea, ":") + 1
        Campoab1 = IIf(a1 > 0, Trim(Mid(Linea, a1, Len(Linea))), "")
        Linea = LineInput(10)
        a1 = InStr(1, Linea, ":") + 1
        Campoac1 = IIf(a1 > 0, Trim(Mid(Linea, a1, Len(Linea))), "")
        Linea = LineInput(10)
        a1 = InStr(1, Linea, ":") + 1
        Campoav1 = IIf(a1 > 0, Trim(Mid(Linea, a1, Len(Linea))), "")
        Linea = LineInput(10)
        a1 = InStr(1, Linea, ":") + 1
        XTipoV = IIf(a1 > 0, Trim(Mid(Linea, a1, Len(Linea))), "")
        Linea = LineInput(10)
        a1 = InStr(1, Linea, ":") + 1
        XClienteO = IIf(a1 > 0, Trim(Mid(Linea, a1, Len(Linea))), "")
        FileClose(10)
        XbdP = xempresa
        CnnString = ("Provider=SQLOLEDB;Data Source=" & XServidorP & ";user id=" & XUsuarioP & "; password=" & XPasswordP & "; Initial Catalog=" & XbdP)
    End Sub
    Public Function Escribir_sql(ByVal mensaje As String)
        FileOpen(15, My.Application.Info.DirectoryPath & "\mensaje.txt", OpenMode.Output)
        WriteLine(15, mensaje)
        FileClose(15)

        Return True
    End Function

    Public Sub Abrir_ini1()
        Dim a1 As Integer
        Dim Linea As String
        Dim XservidorA As String = ""
        Dim XUsuarioa As String = ""
        Dim XPasswordA As String
        Dim XbdA As String = ""

        FileOpen(2, My.Application.Info.DirectoryPath & "\config1.ini", OpenMode.Input)
        Linea = LineInput(2)
        a1 = InStr(1, Linea, ":") + 1
        XservidorA = IIf(a1 > 0, Trim(Mid(Linea, a1, Len(Linea))), "")
        Linea = LineInput(2)
        a1 = InStr(1, Linea, ":") + 1
        XUsuarioa = IIf(a1 > 0, Trim(Mid(Linea, a1, Len(Linea))), "")
        Linea = LineInput(2)
        a1 = InStr(1, Linea, ":") + 1
        XPasswordA = IIf(a1 > 0, Trim(Mid(Linea, a1, Len(Linea))), "")
        Linea = LineInput(2)
        a1 = InStr(1, Linea, ":") + 1
        XbdA = IIf(a1 > 0, Trim(Mid(Linea, a1, Len(Linea))), "")
        FileClose(2)
        CnnStringA = ("Provider=SQLOLEDB;Data Source=" & XservidorA & ";user id=" & XUsuarioa & "; password=" & XPasswordA & "; Initial Catalog=" & XbdA)
    End Sub

    Public Function Agregar_log(ByVal user As String, ByVal codigo As Integer, ByVal op_string As String, ByVal empresa As Integer)
        Dim Cnn As New ADODB.Connection
        Dim SQl1 As String = ""
        Dim Rsta1 As New ADODB.Recordset
        Dim a1 As Integer
        Dim Linea As String = ""
        Dim Xbdp As String = ""
        '        Dim Xbdp1 As String = ""
        Dim XservidorL As String = ""
        Dim XusuarioL As String = ""
        Dim XPasswordl As String = ""
        Dim Xbdl As String = ""
        Dim CnnString1 As String = ""

        FileOpen(3, My.Application.Info.DirectoryPath & "\config1.ini", OpenMode.Input)
        Linea = LineInput(3)
        a1 = InStr(1, Linea, ":") + 1
        XservidorL = IIf(a1 > 0, Trim(Mid(Linea, a1, Len(Linea))), "")
        Linea = LineInput(3)
        a1 = InStr(1, Linea, ":") + 1
        XusuarioL = IIf(a1 > 0, Trim(Mid(Linea, a1, Len(Linea))), "")
        Linea = LineInput(3)
        a1 = InStr(1, Linea, ":") + 1
        XPasswordl = IIf(a1 > 0, Trim(Mid(Linea, a1, Len(Linea))), "")
        Linea = LineInput(3)
        a1 = InStr(1, Linea, ":") + 1
        Xbdl = IIf(a1 > 0, Trim(Mid(Linea, a1, Len(Linea))), "")
        FileClose(3)

        CnnString1 = ("Provider=SQLOLEDB;Data Source=" & XservidorL & ";user id=" & XusuarioL & "; password=" & XPasswordl & "; Initial Catalog=" & Xbdl)

        Cnn.Open(CnnString1)
        empresa = IIf(empresa = 0, 2, empresa)

        SQl1 = "insert into bitx02(op_user,op_fecha_hora,op_codigo,op_string,id_empresa) values('" & XusuarioL & "',convert(datetime,'" & Mid(Now.Date.ToString, 1, 10) & " " & _
                                  Now.Hour & ":" & Now.Minute & "',103)," & codigo & ",'" & op_string & "'," & empresa & ")"
        Rsta1.Open(SQl1, Cnn, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockReadOnly)
        Cnn.Close()

        Return True
    End Function
    Public Function Validar_Linea(ByVal linea As String, ByVal campos As Integer)
        Dim Pc As Integer
        Dim Valida As Boolean = False

        For i = 1 To Len(linea)
            If InStr(Mid(linea, i, 1), ";") Then
                Pc = Pc + 1
            End If
        Next
        If Val(Pc) <> Val(campos) Then
            Valida = True
        End If


        Return Valida
    End Function
    Public Function Valida_BD(ByVal rif As String, ByVal sucursal As Integer)
        Dim ValidaBD(2)
        Dim Cnn As New ADODB.Connection
        Dim SQl1 As String = ""
        Dim Rsta1 As New ADODB.Recordset

        Abrir_ini1()
        Cnn.Open(Variables.CnnStringA)
        SQl1 = "select * from empresa where rif = '" & rif & "' and sucursal=" & sucursal
        Rsta1.Open(SQl1, Cnn, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockReadOnly)
        If Not Rsta1.EOF Then
            ValidaBD(0) = Trim(Rsta1.Fields("id_empresa").Value)
            ValidaBD(1) = Trim(Rsta1.Fields("empresap").Value)
        End If
        '        If Rsta1.Status = 1 Then
        Rsta1.Close()
        'End If
        Cnn.Close()


        Return ValidaBD
    End Function
    Public Function Eliminar_dia(ByVal empresa As String, ByVal dia As Date)
        Dim Cnn As New ADODB.Connection
        Dim SQl1 As String = ""
        Dim Rsta1 As New ADODB.Recordset
        Dim mensaje As String

        Abrir_ini(empresa)
        Cnn.Open(Variables.CnnString)
        SQl1 = "delete from mov_caj where cob_pag in (select cob_num from cobros where fec_cob=convert(datetime,'" & dia & "'103))"
        Rsta1.Open(SQl1, Cnn, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockReadOnly)
        SQl1 = "delete from mov_ban where cob_pag in (select cob_num from cobros where fec_cob=convert(datetime,'" & dia & "'103))"
        Rsta1.Open(SQl1, Cnn, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockReadOnly)
        SQl1 = "delete from reng_tip where cob_num in (select cob_num from cobros where fec_cob=convert(datetime,'" & dia & "'103))"
        Rsta1.Open(SQl1, Cnn, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockReadOnly)
        SQl1 = "delete from reng_cob where cob_num in (select cob_num from cobros where fec_cob=convert(datetime,'" & dia & "'103))"
        Rsta1.Open(SQl1, Cnn, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockReadOnly)
        SQl1 = "delete from cobros where fec_cob=convert(datetime,'" & dia & "'103))"
        Rsta1.Open(SQl1, Cnn, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockReadOnly)
        SQl1 = "delete from docum_cc where fec_emis=convert(datetime,'" & dia & "'103)) and (tipo_doc='FACT' or tipo_ddoc='N/CR' or tipo_doc='N/DB')"
        Rsta1.Open(SQl1, Cnn, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockReadOnly)


        Cnn.Close()
        mensaje = "Eliminación de Infomación de día: " & dia
        Agregar_log(XUsuarioP, 30, mensaje, empresa)


        Return True
    End Function

    Public Function Valida_factura(ByVal nro_factura As String)
        Dim ValidaFactura As Boolean = False
        Dim Cnn As New ADODB.Connection
        Dim SQl1 As String = ""
        Dim Rsta1 As New ADODB.Recordset
        Cnn.Open(Variables.CnnString)
        SQl1 = "select * from docum_cc where tipo_doc='FACT' and nro_doc = '" & nro_factura & "'"
        Rsta1.Open(SQl1, Cnn, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic, ADODB.CommandTypeEnum.adCmdText)
        If Not Rsta1.EOF Then
            ValidaFactura = True
        Else
            Agregar_log(XUsuarioP, 23, "Factura Ya Procesada en Profit Plus: " & nro_factura, frmicgprofit.Xidempresa)
        End If
        Rsta1.Close()
        Cnn.Close()

        Return ValidaFactura
    End Function

    Public Function Validar_Cliente(ByVal cod_cli As String)
        Dim ValidaCliente As Boolean = True
        Dim Cnn As New ADODB.Connection
        Dim SQl1 As String = ""
        Dim Rsta1 As New ADODB.Recordset
        Cnn.Open(Variables.CnnString)
        SQl1 = "select * from clientes where " & Campoac1 & "= '" & cod_cli & "'"
        Rsta1.Open(SQl1, Cnn, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic, ADODB.CommandTypeEnum.adCmdText)
        If Not Rsta1.EOF Then
            ValidaCliente = False
        End If
        Rsta1.Close()
        Cnn.Close()


        Return ValidaCliente
    End Function
    Public Function Crear_Cliente(ByVal cod_cli As String, ByVal nombre_cli As String, ByVal ci_cli As String, ByVal dire_cli As String)
        Dim Cnn As New ADODB.Connection
        Dim SQl1 As String = ""
        Dim Rsta1 As New ADODB.Recordset
        Dim a1 As Integer
        Dim Linea As String
        Dim Xtipo_cli As String = ""
        Dim Xzona As String = ""
        Dim Xsegmento As String = ""
        Dim XVendedor As String = ""
        Dim XCtaIng As String = ""
        Dim XPais As String = ""
        Dim Xrif As String = ""
        Dim creas As Boolean = False
        Dim XCo_Cli As String


        Cnn.Open(Variables.CnnString)

        FileOpen(2, My.Application.Info.DirectoryPath & "\clientes.ini", OpenMode.Input)
        Linea = LineInput(2)
        a1 = InStr(1, Linea, ":") + 1
        Xtipo_cli = IIf(a1 > 0, Trim(Mid(Linea, a1, Len(Linea))), "")
        Linea = LineInput(2)
        a1 = InStr(1, Linea, ":") + 1
        Xzona = IIf(a1 > 0, Trim(Mid(Linea, a1, Len(Linea))), "")
        Linea = LineInput(2)
        a1 = InStr(1, Linea, ":") + 1
        Xsegmento = IIf(a1 > 0, Trim(Mid(Linea, a1, Len(Linea))), "")
        Linea = LineInput(2)
        a1 = InStr(1, Linea, ":") + 1
        XVendedor = IIf(a1 > 0, Trim(Mid(Linea, a1, Len(Linea))), "")
        Linea = LineInput(2)
        a1 = InStr(1, Linea, ":") + 1
        XCtaIng = IIf(a1 > 0, Trim(Mid(Linea, a1, Len(Linea))), "")
        Linea = LineInput(2)
        a1 = InStr(1, Linea, ":") + 1
        XPais = IIf(a1 > 0, Trim(Mid(Linea, a1, Len(Linea))), "")
        FileClose(2)
        Xrif = IIf(Mid(Trim(ci_cli), 1, 1) <> "V", Trim(ci_cli), "")
        SQl1 = "select * from clientes where co_cli='" & Trim(ci_cli) & "'"
        Rsta1.Open(SQl1, Cnn, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic, ADODB.CommandTypeEnum.adCmdText)
        If Not Rsta1.EOF Then
            creas = True
            XCo_Cli = Rsta1.Fields("co_cli").Value
        End If
        Rsta1.Close()

        If creas = False Then
            SQl1 = "insert into clientes (co_cli,tipo,cli_des,direc1,fecha_reg,fec_ult_ve,co_zon,co_seg,co_ven,rif,co_ingr," & Campoac1 & "," & _
                    "co_us_in,fe_us_in,fe_us_mo,fe_us_el,co_sucu,estado,co_pais) values('" & Trim(ci_cli) & "','" & Xtipo_cli & "','" & _
                    Trim(nombre_cli) & "','" & dire_cli & "',convert(datetime,'" & Now.Date & "',103),convert(datetime,'" & Now.Date & "',103),'" & _
                    Xzona & "','" & Xsegmento & "','" & XVendedor & "','" & _
                    Xrif & "','" & XCtaIng & "','" & cod_cli & "','" & Xusuario & "',convert(datetime,'" & Now.Date & "',103),convert(datetime,'" & _
                    Now.Date & "',103), convert(datetime,'" & Now.Date & "',103),'" & Xsucursal & "','A','" & XPais & "')"
            '        Escribir_sql(SQl1)
            Rsta1.Open(SQl1, Cnn, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic, ADODB.CommandTypeEnum.adCmdText)
            Agregar_log(XUsuarioP, 20, "Cliente Creado: " & Trim(ci_cli) & " Nombre: " & Trim(nombre_cli), frmicgprofit.Xidempresa)
        End If
        Cnn.Close()
        Return XCo_Cli
    End Function
    Public Function Crear_Vendedor(ByVal co_ven As String, ByVal nombre_ven As String, ByVal ci_ven As String)
        Dim Cnn As New ADODB.Connection
        Dim SQl1 As String = ""
        Dim Rsta1 As New ADODB.Recordset



        Cnn.Open(Variables.CnnString)
        SQl1 = "insert into vendedor(co_ven,tipo,ven_des,dis_cen,cedula,direc1,direc2,telefonos,fecha_reg,condic,comision," & _
                "comen,fun_cob,fun_ven,comisionv,fac_ult_ve,fec_ult_ve,net_ult_ve,cli_ult_ve,cta_contab,campo1,campo2,campo3,campo4,campo5,campo6," & _
                "campo7,campo8,co_us_in,fe_us_in,co_us_mo,fe_us_mo,co_us_el,fe_us_el,revisado,trasnfe,co_sucu) values('" & Trim(co_ven) & "','A'," & _
                "'" & Replace(nombre_ven, "'", "''") & "','','" & ci_ven & "','','','',convert(datetime,'" & Now.Date & "',103),0,0," & _
                "'',0,0,0,0,convert(datetime,'" & Now.Date & "',103),0,'','','','','','','','" & Trim(co_ven) & "','','','" & XUsuarioP & "',convert(datetime,'" & _
                Now.Date & "',103),'',convert(datetime,'" & Now.Date & "',103),'',convert(datetime,'" & Now.Date & "',103),'','','" & Xsucursal & "')"

        Rsta1.Open(SQl1, Cnn, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic, ADODB.CommandTypeEnum.adCmdText)
        Agregar_log(XUsuarioP, 26, "Vendedor Creado: " & Trim(co_ven) & " Nombre: " & Trim(nombre_ven), frmicgprofit.Xidempresa)
        Cnn.Close()
        Return True
    End Function

    Public Function Buscar_Cliente_Factura(ByVal nro_factura As String)
        Dim Cnn As New ADODB.Connection
        Dim SQl1 As String = ""
        Dim Rsta1 As New ADODB.Recordset
        Dim datos_cli(6) As String

        Cnn.Open(Variables.CnnString)
        SQl1 = "select * from docum_cc where tipo_doc='FACT' and nro_doc = '" & nro_factura & "'"
        Rsta1.Open(SQl1, Cnn, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic, ADODB.CommandTypeEnum.adCmdText)
        If Not Rsta1.EOF Then
            datos_cli(0) = Rsta1.Fields("co_cli").Value
            datos_cli(1) = Rsta1.Fields("nro_doc").Value
            datos_cli(2) = Rsta1.Fields("co_ven").Value
            datos_cli(3) = Rsta1.Fields("campo1").Value
            datos_cli(4) = Rsta1.Fields("campo2").Value
            datos_cli(5) = Rsta1.Fields("campo3").Value

            'ValidaVendedor = False
        End If
        '        If Rsta1.Status = 1 Then
        Rsta1.Close()
        '        End If
        Cnn.Close()

        Return datos_cli
    End Function
    Public Function CNota_credito(ByVal n_credito As Integer, ByVal nro_factura As String, ByVal fec_emis As String, ByVal co_cli As String, _
                                        ByVal co_ven As String, ByVal monto_bru As String, _
                                       ByVal iva As String, ByVal monto_net As String, ByVal tasa As Integer, _
                                       ByVal numcon As String, ByVal impfis As String, ByVal nom_cli As String, ByVal ci_cli As String, ByVal dire_cli As String)
        Dim Cnn As New ADODB.Connection
        Dim SQl1 As String = ""
        Dim Rsta1 As New ADODB.Recordset
        Dim XDis_cen As String
        Dim Nro_Factura1 As Integer
        Dim XObserva As String = ""


        Cnn.Open(CnnString)
        SQl1 = "select * from docum_cc where tipo_doc='N/CR' and nro_doc= " & n_credito
        Rsta1.Open(SQl1, Cnn, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic, ADODB.CommandTypeEnum.adCmdText)
        If Not Rsta1.EOF Then
            Agregar_log(XUsuarioP, 31, "Nota de credto: " & n_credito & ", usada en Profit", frmicgprofit.Xidempresa)
        Else
            Nro_Factura1 = n_credito
            Rsta1.Close()

            XDis_cen = "<IVA><1>" & Format(tasa, "##0.00") & "/" & Replace(CDbl(monto_bru), ",", ".") & "/" & Replace(CDbl(iva), ",", ".") & "</1><IVA>"

            XObserva = "Nota de Credito por Factura: " & Nro_Factura1
            SQl1 = "insert into docum_cc (tipo_doc,nro_doc,anulado,co_cli, fec_emis,fec_venc,observa,doc_orig,nro_orig,co_ven,tipo,tasa,moneda,monto_imp,monto_bru," & _
                    " monto_net,saldo,feccom,dis_cen,campo1,campo2,campo3,campo8,co_us_in,fe_us_in,fe_us_mo,fe_us_el,numcon,co_sucu,fec_reg,impfis,impfisfac) values('N/CR'," & _
                    Nro_Factura1 & ",0,'" & ci_cli & "'," & _
                    "convert(datetime,'" & fec_emis & "',103), convert(datetime,'" & fec_emis & "',103),'" & XObserva & "','FACT'," & nro_factura & ",'" & co_ven & "',1,1,'" & XMoneda & "'," & _
                    Replace(Val(iva), ",", ".") & "," & Replace(Val(monto_bru), ",", ".") & "," & _
                    Replace(Val(monto_net), ",", ".") & "," & Replace(Val(monto_net), ",", ".") & ",convert(datetime,'" & _
                    fec_emis & "',103),'" & XDis_cen & "','" & Replace(nom_cli, "'", "''") & "','" & ci_cli & "','" & dire_cli & "','AUTOMATICO','" & Xusuario & "',convert(datetime,'" & Now.Date & "',103),convert(datetime,'" & _
                    Now.Date & "',103),convert(datetime,'" & Now.Date & "',103),'" & numcon & "','" & Xsucursal & "',convert(datetime,'" & Now.Date & "',103),'" & impfis & "','" & nro_factura & "')"
            Rsta1.Open(SQl1, Cnn, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic, ADODB.CommandTypeEnum.adCmdText)
            Agregar_log(XUsuarioP, 22, "Creada " & XObserva, frmicgprofit.Xidempresa)
        End If
        Cnn.Close()
        Return Nro_Factura1
    End Function
    Public Function Validar_Vendedor(ByVal cod_ven As String)
        Dim ValidaVendedor As Boolean = True
        Dim Cnn As New ADODB.Connection
        Dim SQl1 As String = ""
        Dim Rsta1 As New ADODB.Recordset
        Cnn.Open(Variables.CnnString)
        SQl1 = "select * from vendedor where " & Campoav1 & "= '" & cod_ven & "'"
        Rsta1.Open(SQl1, Cnn, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic, ADODB.CommandTypeEnum.adCmdText)
        If Not Rsta1.EOF Then
            ValidaVendedor = False
        Else

        End If
        '        If Rsta1.Status = 1 Then
        Rsta1.Close()
        '        End If
        Cnn.Close()

        Return ValidaVendedor
    End Function
    Public Function Encabezado_Factura(ByVal nro_factura As String, ByVal fec_emis As String, ByVal co_cli As String, _
                                       ByVal nom_cli As String, ByVal ci_cli As String, ByVal dire_cli As String, ByVal co_ven As String, _
                                       ByVal monto_bru As String, ByVal monto_des As String, _
                                       ByVal iva As String, ByVal monto_net As String, ByVal tasa As Integer, _
                                       ByVal impfis As String)
        Dim Cnn As New ADODB.Connection
        Dim SQl1 As String = ""
        Dim Rsta1 As New ADODB.Recordset
        Dim XDis_cen As String
        Dim NumCon As Integer

        XDis_cen = "<IVA><1>" & tasa & "/" & Replace(Val(monto_bru), ",", ".") & "/" & Replace(Val(iva), ",", ".") & "</1><IVA>"

        Cnn.Open(Variables.CnnString)
        NumCon = 0
        SQl1 = "insert into docum_cc (tipo_doc,nro_doc,anulado,co_cli, fec_emis,fec_venc,co_ban,co_ven,tipo,tasa,moneda,monto_imp,monto_bru,descuentos,monto_des," & _
                " monto_net,saldo,feccom,dis_cen,campo1,campo2,campo3,co_us_in,fe_us_in,fe_us_mo,fe_us_el,numcon,co_sucu,fec_reg,impfis,impfisfac) values('FACT'," & _
                nro_factura & ",0,'" & co_cli & "'," & _
                "convert(datetime,'" & fec_emis & "',103), convert(datetime,'" & fec_emis & "',103),'1122','" & co_ven & "',1,1,'" & XMoneda & "'," & _
                Replace(Val(iva), ",", ".") & "," & Replace(Val(monto_bru), ",", ".") & "," & Replace(FormatNumber(((Val(monto_des) / (IIf(monto_bru > 0, Val(monto_bru), 1))) * 100), 2), ",", ".") & _
                "," & Replace(Val(monto_des), ",", ".") & "," & Replace(Val(monto_net), ",", ".") & "," & Replace(Val(monto_net), ",", ".") & ",convert(datetime,'" & _
                fec_emis & "',103),'" & XDis_cen & "','" & Replace(nom_cli, "'", "''") & "','" & ci_cli & "','" & dire_cli & "','" & Xusuario & "',convert(datetime,'" & Now.Date & "',103),convert(datetime,'" & _
                Now.Date & "',103),convert(datetime,'" & Now.Date & "',103),'" & NumCon & "','" & Xsucursal & "',convert(datetime,'" & Now.Date & "',103),'" & impfis & "','" & nro_factura & "')"
        Escribir_sql(SQl1)
        Rsta1.Open(SQl1, Cnn, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic, ADODB.CommandTypeEnum.adCmdText)

        Cnn.Close()
        Return True
    End Function
    Public Function Cobros(ByVal fact_num As Integer, ByVal co_cli As String, ByVal co_ven As String, ByVal fec_cob As String, ByVal monto_cob As String, ByVal monto_fac As String, ByVal monto_iva As String)
        Dim Cnn As New ADODB.Connection
        Dim SQl1 As String = ""
        Dim Rsta1 As New ADODB.Recordset
        Dim SQl2 As String = ""
        Dim Rsta2 As New ADODB.Recordset
        Dim xcobro As Integer
        Dim XDescripcion As String
        Dim Reng As Integer = 0
        Dim ValidaF As Boolean = False


        Cnn.Open(Variables.CnnString)
        SQl1 = "select * from docum_cc where saldo>0 and tipo_doc='FACT' and nro_doc=" & fact_num
        Rsta1.Open(SQl1, Cnn, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic, ADODB.CommandTypeEnum.adCmdText)
        If Not Rsta1.EOF Then
            ValidaF = True
        End If
        Rsta1.Close()

        If ValidaF = True Then
            SQl1 = "Select max(cob_num) as numero from cobros"
            Rsta1.Open(SQl1, Cnn, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic, ADODB.CommandTypeEnum.adCmdText)
            If Not Rsta1.EOF Then
                xcobro = Rsta1.Fields("numero").Value + 1
            Else
                xcobro = 0
            End If
            Rsta1.Close()

            XDescripcion = "Cobro nro:" & xcobro & " de Factura:" & fact_num
            'insert en cobros
            SQl1 = "insert into cobros (cob_num,co_cli,co_ven,fec_cob,anulado,monto,feccom,tasa,moneda,campo8,co_us_in,fe_us_in," & _
                    "fe_us_mo,fe_us_el,co_sucu,descrip) values (" & xcobro & ",'" & co_cli & "','" & co_ven & "',convert(datetime,'" & _
                    fec_cob & "',103),0,0,convert(datetime,'" & Now.Date & "',103),1,'" & XMoneda & "','AUTOMATICO','" & Xusuario & "'," & _
                    "convert(datetime,'" & Now.Date & "',103),convert(datetime,'" & Now.Date & "',103),convert(datetime,'" & Now.Date & "',103),'" & _
                    Xsucursal & "','" & XDescripcion & "')"

            Rsta1.Open(SQl1, Cnn, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic, ADODB.CommandTypeEnum.adCmdText)

            'insert en renglones de cobros

            Reng = Reng + 1
            SQl1 = "insert into reng_cob(cob_num,reng_num,tp_doc_cob,doc_num,neto,mont_cob,imp_pago,isv,moneda,tasa,fec_emis,fec_venc) values(" & _
                    xcobro & "," & Reng & ",'FACT'," & fact_num & "," & Replace(Val(monto_fac), ",", ".") & "," & Replace(Val(monto_fac), ",", ".") & "," & _
                    Replace(Val(monto_iva), ",", ".") & "," & Replace(Val(monto_iva), ",", ".") & ",'" & XMoneda & "',1,convert(datetime,'" & Now.Date & "',103)," & _
                    "Convert(datetime,'" & Now.Date & "',103))"
            Rsta1.Open(SQl1, Cnn, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic, ADODB.CommandTypeEnum.adCmdText)

            'busca notas de credito
            SQl1 = "select * from docum_cc where tipo_doc='N/CR' and saldo>0 and co_cli='" & co_cli & "'"
            Rsta1.Open(SQl1, Cnn, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic, ADODB.CommandTypeEnum.adCmdText)
            If Not Rsta1.EOF Then
                Reng = Reng + 1
                SQl2 = "insert into reng_cob (cob_num,reng_num,tp_doc_cob,doc_num,neto,mont_cob,imp_pago,isv,moneda,tasa,fec_emis,fec_venc) values(" & _
                    xcobro & "," & Reng & ",'N/CR'," & Rsta1.Fields("nro_doc").Value & "," & Replace(Rsta1.Fields("saldo").Value, ",", ".") & "," & _
                    Replace(Rsta1.Fields("saldo").Value, ",", ".") & "," & Replace(Rsta1.Fields("monto_imp").Value, ",", ".") & "," & Replace(Rsta1.Fields("monto_imp").Value, ",", ".") & _
                    ",'" & XMoneda & "',1,convert(datetime,'" & Now.Date & "',103)," & _
                    "Convert(datetime,'" & Now.Date & "',103))"
                Rsta2.Open(SQl2, Cnn, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic, ADODB.CommandTypeEnum.adCmdText)
                SQl2 = "update docum_cc set saldo=0 where tipo_doc='N/CR' and co_cli='" & co_cli & "' and nro_doc=" & Rsta1.Fields("nro_doc").Value
                Rsta2.Open(SQl2, Cnn, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic, ADODB.CommandTypeEnum.adCmdText)
                SQl2 = "update reng_cob set sino_pago=1, monto_dppago=" & Replace(Rsta1.Fields("saldo").Value, ",", ".") & _
                        " where cob_num=" & xcobro & " and reng_num=1"
                Rsta2.Open(SQl2, Cnn, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic, ADODB.CommandTypeEnum.adCmdText)
                Agregar_log(XUsuarioP, 25, "Procesada Nro de Credito: " & Rsta1.Fields("nro_doc").Value & " A Cobro: " & xcobro, frmicgprofit.Xidempresa)
            Else
                Agregar_log(XUsuarioP, 24, "Factura: " & fact_num & " Ya Cobrada", frmicgprofit.Xidempresa)
            End If
            Rsta1.Close()

            'inserta en reng_tip

            'inserta en mov_ban o mov_caj

            Cnn.Close()
        End If
        Return xcobro
    End Function
    Public Function Ingreso_Tips(ByVal cob_num As Integer, ByVal renglon As Integer, ByVal tipo_cob As String, ByVal fecha As String, ByVal monto_cob As String, ByVal cod_caja As String, ByVal banco As String, ByVal num_doc As String, ByVal nro_fac As Integer)
        Dim Cnn As New ADODB.Connection
        Dim SQl1 As String = ""
        Dim Rsta1 As New ADODB.Recordset

        Dim XMovi As Integer
        Dim XDes_caja As String
        Dim XNombre_Ban As String
        Dim Xsaldo As String
        Dim XsaldoC As String
        Dim XBanco As String = ""

        Cnn.Open(Variables.CnnString)
        SQl1 = "select cod_caja,descrip from cajas where cod_caja='" & cod_caja & "'"
        Rsta1.Open(SQl1, Cnn, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic, ADODB.CommandTypeEnum.adCmdText)
        If Not Rsta1.EOF Then
            XDes_caja = Rsta1.Fields("descrip").Value
        Else
            XDes_caja = ""
        End If
        Rsta1.Close()
        If tipo_cob = "TARJ" Or tipo_cob = "CHEQ" Then
            If tipo_cob = "TARJ" Then
                SQl1 = "select co_tar as co_ban,des_tar as des_ban from tarj_cre where co_tar='" & cod_caja & "'"
            ElseIf tipo_cob = "CHEQ" Then
                SQl1 = "select co_ban,des_ban from bancos where co_ban='" & banco & "'"
            End If
            Rsta1.Open(SQl1, Cnn, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic, ADODB.CommandTypeEnum.adCmdText)
            If Not Rsta1.EOF Then
                XNombre_Ban = Rsta1.Fields("des_ban").Value
            Else
                XNombre_Ban = ""
            End If
            Rsta1.Close()
        Else
            XNombre_Ban = ""
        End If
        If tipo_cob = "DEPO" Or tipo_cob = "TARJ" Then
            'genera un movimiento en banco
            XBanco = Buscar_Banco(banco)

            XMovi = Mov_ban(XBanco, monto_cob, cob_num, fecha, num_doc)
        Else
            ' genera un movimiento de caja
            XMovi = Mov_Caja(tipo_cob, fecha, num_doc, monto_cob, cob_num, banco, cod_caja)

        End If

        SQl1 = "insert into reng_tip(cob_num,reng_num,tip_cob,movi,num_doc,mont_doc,mont_tmp,moneda,banco,cod_caja,des_caja,fec_cheq,nombre_ban) values (" & _
            cob_num & "," & renglon & ",'" & tipo_cob & "'," & XMovi & ",'" & num_doc & "'," & Replace(monto_cob, ",", ".") & "," & _
            Replace(monto_cob, ",", ".") & ",'" & Variables.XMoneda & "','" & IIf(tipo_cob = "DEPO", "", banco) & "','" & IIf(tipo_cob = "DEPO", banco, cod_caja) & "','" & XDes_caja & "',convert(datetime,'" & _
            fecha & "',103),'" & XNombre_Ban & "')"
        Rsta1.Open(SQl1, Cnn, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic, ADODB.CommandTypeEnum.adCmdText)

        'Inserta respaldo de forma de cobro
        SQl1 = "insert into reng_tipr(cob_num,reng_num,tip_cob,movi,num_doc,mont_doc,mont_tmp,moneda,banco,cod_caja,des_caja,fec_cheq,nombre_ban) values (" & _
            cob_num & "," & renglon & ",'" & tipo_cob & "'," & XMovi & ",'" & num_doc & "'," & Replace(monto_cob, ",", ".") & "," & _
            Replace(monto_cob, ",", ".") & ",'" & Variables.XMoneda & "','" & IIf(tipo_cob = "DEPO", "", banco) & "','" & IIf(tipo_cob = "DEPO", banco, cod_caja) & "','" & XDes_caja & "',convert(datetime,'" & _
            fecha & "',103),'" & XNombre_Ban & "')"
        Rsta1.Open(SQl1, Cnn, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic, ADODB.CommandTypeEnum.adCmdText)

        ' se debe actualizar el cobro y el renglo de cobro
        ''''' Aqui esta malo hay que tomar en cuenta la nota de credito en los reglones para tomar el saldo del cobro
        ''''*****************************************************************************************************************************
        XsaldoC = 0
        XsaldoC = Val(Reng_CobroS(cob_num))
        SQl1 = "update cobros set monto=" & Replace(XsaldoC, ",", ".") & " where cob_num=" & cob_num
        Rsta1.Open(SQl1, Cnn, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic, ADODB.CommandTypeEnum.adCmdText)
        '

        '        Xsaldo = Val(Reng_CobroS(cob_num)) + monto_cob
        '        SQl1 = "update reng_cob set neto=" & Replace(Xsaldo, ",", ".") & " where cob_num=" & cob_num
        '        Rsta1.Open(SQl1, Cnn, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic, ADODB.CommandTypeEnum.adCmdText)

        'Actualiza el saldo en la factura

        Xsaldo = 0
        Xsaldo = Val(FacturaS(nro_fac))
        Xsaldo = IIf(Xsaldo > 0, Xsaldo - XsaldoC, 0)
        SQl1 = "update docum_cc set saldo=" & Replace(Xsaldo, ",", ".") & " where tipo_doc='FACT' and nro_doc=" & nro_fac
        Rsta1.Open(SQl1, Cnn, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic, ADODB.CommandTypeEnum.adCmdText)

        Cnn.Close()
        Return 1
    End Function
    Public Function CobroS(ByVal cob_num As Integer)
        Dim Cnn As New ADODB.Connection
        Dim SQl1 As String = ""
        Dim Rsta1 As New ADODB.Recordset
        Dim Xsaldo As String
        Cnn.Open(Variables.CnnString)
        SQl1 = "select cob_num,monto from cobros where cob_num=" & cob_num
        Rsta1.Open(SQl1, Cnn, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic, ADODB.CommandTypeEnum.adCmdText)
        If Not Rsta1.EOF Then
            Xsaldo = Rsta1.Fields("monto").Value
        End If
        Cnn.Close()

        Return Xsaldo
    End Function
    Public Function Buscar_Banco(ByVal banco As Integer)
        Dim Cnn As New ADODB.Connection
        Dim SQl1 As String = ""
        Dim Rsta1 As New ADODB.Recordset
        Dim BAnco1 As String = ""
        '        Dim Xsaldo As Decimal = 0
        Cnn.Open(Variables.CnnString)
        SQl1 = "select cod_cta from cuentas where " & Campoab1 & "='" & banco & "'"
        Rsta1.Open(SQl1, Cnn, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic, ADODB.CommandTypeEnum.adCmdText)
        If Not Rsta1.EOF Then
            BAnco1 = Rsta1.Fields("cod_cta").Value
        End If
        Cnn.Close()

        Return BAnco1
    End Function
    Public Function Reng_CobroS(ByVal cob_num As Integer)
        Dim Cnn As New ADODB.Connection
        Dim SQl1 As String = ""
        Dim Rsta1 As New ADODB.Recordset
        Dim Xsaldo As String
        Cnn.Open(Variables.CnnString)
        SQl1 = "select cob_num,tp_doc_cob,neto from reng_cob where cob_num=" & cob_num '& " and tp_doc_cob<>'N/CR'"
        Rsta1.Open(SQl1, Cnn, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic, ADODB.CommandTypeEnum.adCmdText)
        While Not Rsta1.EOF
            Xsaldo = Xsaldo + IIf(Rsta1.Fields("tp_doc_cob").Value = "N/CR", -1, 1) * Rsta1.Fields("neto").Value
            Rsta1.MoveNext()
        End While
        Cnn.Close()

        Return Xsaldo
    End Function
    Public Function FacturaS(ByVal nro_fac As Integer)
        Dim Cnn As New ADODB.Connection
        Dim SQl1 As String = ""
        Dim Rsta1 As New ADODB.Recordset
        Dim Xsaldo As String
        Cnn.Open(Variables.CnnString)
        SQl1 = "select tipo_doc,nro_doc,saldo from docum_cc where tipo_doc='FACT' and nro_doc=" & nro_fac
        Rsta1.Open(SQl1, Cnn, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic, ADODB.CommandTypeEnum.adCmdText)
        If Not Rsta1.EOF Then
            Xsaldo = Rsta1.Fields("saldo").Value
        End If
        Cnn.Close()

        Return Xsaldo
    End Function
    Public Function Mov_Caja(ByVal forma_pag As String, ByVal fecha As String, ByVal doc_num As String, ByVal monto As String, ByVal cob_pag As Integer, ByVal banc_tarj As String, ByVal cod_caja As String)
        Dim Cnn As New ADODB.Connection
        Dim SQl1 As String = ""
        Dim Rsta1 As New ADODB.Recordset

        Dim Xforma_Pag As String = ""
        Dim XMov_caj As Integer
        Dim XDescripcion As String = ""


        Select Case forma_pag
            Case Is = "TARJ"
                Xforma_Pag = "TJ"
            Case Is = "CHEQ"
                Xforma_Pag = "CH"
            Case Is = "EFEC"
                Xforma_Pag = "EF"
        End Select

        Cnn.Open(Variables.CnnString)
        SQl1 = "select max(mov_num) as numero from mov_caj"
        Rsta1.Open(SQl1, Cnn, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic, ADODB.CommandTypeEnum.adCmdText)
        If Not Rsta1.EOF Then
            XMov_caj = Rsta1.Fields("numero").Value + 1
        Else
            XMov_caj = 1
        End If
        Rsta1.Close()

        SQl1 = "insert into mov_caj(mov_num,codigo,dep_num,reng_num,origen,tipo_op,forma_pag,fecha,doc_num,descrip,monto_d,monto_h,cta_egre,cob_pag,ori_dep," & _
             "banc_tarj,fecha_che,feccom,moneda,tasa,co_us_in,fe_us_in,fe_us_mo,fe_us_el,co_sucu,anulado) values(" & XMov_caj & ",'" & cod_caja & "',0,0,'COB','I','" & _
             Xforma_Pag & "',convert(datetime,'" & fecha & "',103), '" & doc_num & "','" & XDescripcion & "',0," & Replace(monto, ",", ".") & ",'" & Variables.XCta_ingreso & "'," & _
             cob_pag & ",1,'" & banc_tarj & "',convert(datetime,'" & fecha & "',103),convert(datetime,'" & fecha & "',103),'" & Variables.XMoneda & "',1,'" & _
             Variables.Xusuario & "',convert(datetime,'" & Now.Date & "',103),convert(datetime,'" & Now.Date & "',103),convert(datetime,'" & Now.Date & "',103),'" & _
             Variables.Xsucursal & "',0)"
        Rsta1.Open(SQl1, Cnn, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic, ADODB.CommandTypeEnum.adCmdText)

        Cnn.Close()

        Return XMov_caj
    End Function
    Public Function Mov_ban(ByVal codigo As String, ByVal monto As String, ByVal cobro As Integer, ByVal fecha As String, ByVal num_doc As String)
        Dim Cnn As New ADODB.Connection
        Dim SQl1 As String = ""
        Dim Rsta1 As New ADODB.Recordset
        Dim XMov_ban As Integer = 0
        Dim XDescripcion As String = ""


        Cnn.Open(Variables.CnnString)

        SQl1 = "select max(mov_num) as numero from mov_ban"
        Rsta1.Open(SQl1, Cnn, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic, ADODB.CommandTypeEnum.adCmdText)
        If Not Rsta1.EOF Then
            XMov_ban = Rsta1.Fields("numero").Value + 1
        Else
            XMov_ban = 1
        End If
        Rsta1.Close()

        SQl1 = "insert into mov_ban (mov_num,codigo,dep_num,reng_num,origen,tipo_op,fecha,doc_num,descrip,monto_d,monto_h,cta_egre," & _
             " cob_pag,ori_dep,fec_con,fecha_che,feccom,moneda,tasa,co_us_in,fe_us_in,fe_us_mo,fe_us_el,co_sucu,anulado) values (" & _
             XMov_ban & ",'" & codigo & "',0,0,'BAN','DP',convert(datetime,'" & fecha & "',103),'" & num_doc & "','" & XDescripcion & "',0," & _
             Replace(monto, ",", ".") & ",'" & XCta_ingreso & "'," & cobro & ",1,convert(datetime,'" & fecha & "',103),convert(datetime,'" & _
             fecha & "',103),convert(datetime,'" & Now.Date & "',103),'" & Variables.XMoneda & "',1,'" & Variables.Xusuario & "',convert(datetime,'" & Now.Date & "',103)," & _
             "convert(datetime,'" & Now.Date & "',103),convert(datetime,'" & Now.Date & "',103),'" & Xsucursal & "',0)"
        Rsta1.Open(SQl1, Cnn, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic, ADODB.CommandTypeEnum.adCmdText)

        Cnn.Close()
        Return XMov_ban
    End Function
End Module
