﻿'Imports Finisar.SQLite
Imports System.Data.SQLite
Imports System.IO
Public Class frmconfiguracion

    Private Sub frmconfiguracion_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim Consulta As String = ""
        Dim Cnn As New SQLiteConnection("Data Source=profit-icg.db;Version=3;")
        Dim ObjCommand As SQLiteCommand
        Dim ObjReader As SQLiteDataReader
        'Dim Ada As New SQLiteDataAdapter()
        '        Dim Sqlstr As New SQLiteCommand(Consulta, Cnn)
        Dim X1 As Integer = 1


        '        Dim carpeta As String = My.Computer.FileSystem.CurrentDirectory
        '        Cnn = New SQLiteConnection("Data Source=" & Application.StartupPath & "\profit-icg.db;Version=3;")

        Try
            If (Not File.Exists("profit-icg.db")) Then
                MsgBox("No se consigue la BD", MsgBoxStyle.Information, "mensaje")
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error")
        End Try

        Try
            Cnn.Open()

            '            If (File.Exists("profit-icg.db")) Then
            Consulta = String.Format("SELECT * FROM empresa")
            ObjCommand = Cnn.CreateCommand
            ObjCommand.CommandText = Consulta

            ObjReader = ObjCommand.ExecuteReader
            dgv1.Rows.Clear()
            dgv1.ColumnCount = 4
            dgv1.Columns(0).Name = "Empresa"
            dgv1.Columns(1).Name = "Directorio Arc"
            dgv1.Columns(2).Name = "Nombre Arc"
            dgv1.Columns(3).Name = "ID_Empresa"
            dgv1.Columns(3).Visible = False

            dgv1.Rows.Add(5)

            'Dim dr As SQLiteDataReader = Sqlstr.ExecuteReader(CommandBehavior.CloseConnection)
            'While dr.Read
            While ObjReader.Read()
                '           Dim Valor1 As Object = dr.Item("cod_profit")
                '            Dim Valor2 As Object = dr.Item("directori_arc")
                '          Dim Valor3 As Object = dr.Item("archivo_arc")
                dgv1.Item(0, X1).Value = ObjReader("cod_profit")
                dgv1.Item(1, X1).Value = ObjReader("dir_archivo")
                dgv1.Item(2, X1).Value = ObjReader("nom_archivo")
                dgv1.Item(3, X1).Value = ObjReader("id_empresa")
                dgv1.Rows.Add()
                X1 = X1 + 1
            End While
            dgv1.MultiSelect = True
            dgv1.Sort(dgv1.Columns(0), System.ComponentModel.ListSortDirection.Ascending)
            For i = 0 To dgv1.RowCount - 2
                If dgv1.Item(0, i).Value = "" Then
                    dgv1.Rows(i).Selected = True
                End If
            Next

            Dim Value As DataGridViewSelectedRowCollection
            Value = dgv1.SelectedRows
            For Each vfila As DataGridViewRow In Value
                dgv1.Rows.Remove(vfila)
            Next

            dgv1.MultiSelect = False
            '        dr.Close()
            'End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error")
        Finally
            If Not IsNothing(Cnn) Then
                Cnn.Close()
            End If
        End Try


    End Sub

    Private Sub dgv1_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgv1.CellContentClick

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim Frm As New frmconfig1
        Dim i As Integer

        i = dgv1.CurrentRow.Index
        Frm.proceso = 1
        Frm.ShowDialog()
        frmconfiguracion_Load(sender, e)


    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Me.Close()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim Frm As New frmconfig1
        Dim i As Integer

        i = dgv1.CurrentRow.Index
        Frm.proceso = 2
        Frm.Empresa = dgv1(3, i).Value
        Frm.ShowDialog()
        frmconfiguracion_Load(sender, e)

    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Dim Consulta As String = ""
        Dim Cnn As New SQLiteConnection("Data Source=profit-icg.db;Version=3;")
        Dim ObjCommand As SQLiteCommand
        Dim ObjReader As SQLiteDataReader
        Dim i As Integer
        Dim empresa As Integer

        Dim Resp1 As Integer
        i = dgv1.CurrentRow.Index
        Empresa = dgv1(3, i).Value

        Resp1 = MsgBox("Esta Seguro que desea Eliminar este registro?", MsgBoxStyle.YesNo, "Mensaje de Solicitud")
        If Resp1 = 6 Then
            Cnn.Open()
            Consulta = String.Format("delete from empresa where id_empresa=" & Empresa)


            ObjCommand = Cnn.CreateCommand
            ObjCommand.CommandText = Consulta
            ObjReader = ObjCommand.ExecuteReader

            Cnn.Close()
        End If
        frmconfiguracion_Load(sender, e)



    End Sub
End Class