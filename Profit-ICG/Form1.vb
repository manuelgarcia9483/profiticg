﻿
Public Class frmicgprofit
    Public Xempresa As String = ""
    Public Xidempresa As Integer = 0

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim Empresa As String
        Dim dia As Date
        Button1.Visible = False
        '' La Interface va tener tres argumentos 
        '' el Primero 0 o 1 para saber si es 0 automatico o 1 manual por profit
        '' el segundo parametro si el primero es 0 va a ser el nombre del archivo con ruta completa y si es 1 en nombre de la BD
        '' el tercer parametro es el usuario
        Dim SiExiste As Boolean
        If My.Application.CommandLineArgs.Count = 3 Then
            If Trim(My.Application.CommandLineArgs(0)) = 0 Then
                Variables.XArchivo = Trim(My.Application.CommandLineArgs(1))
                Variables.Xusuario = Trim(My.Application.CommandLineArgs(2))
                SiExiste = System.IO.File.Exists(Variables.XArchivo)
                If SiExiste = False Then
                    Agregar_log(Variables.Xusuario, 5, "Archivo remitido no existe", "")
                Else
                    Me.TextBox1.Text = Variables.XArchivo
                    Button2_Click(sender, e)
                End If
                Me.Close()
            Else
                If Trim(My.Application.CommandLineArgs(0)) = 2 Then
                    '                    Variables.XArchivo = Trim(My.Application.CommandLineArgs(1))
                    '                   Variables.Xusuario = Trim(My.Application.CommandLineArgs(2))
                    '                  Eliminar_dia(empresa, dia)
                    Dim Frm1 As New frmeliminar

                    Frm1.ShowDialog()

                Else

                    Variables.XbdP = Trim(My.Application.CommandLineArgs(1))
                End If
            End If
            Variables.Xusuario = Trim(My.Application.CommandLineArgs(2))
        Else
            If Trim(My.Application.CommandLineArgs(0)) = 2 Then
                '                    Variables.XArchivo = Trim(My.Application.CommandLineArgs(1))
                '                   Variables.Xusuario = Trim(My.Application.CommandLineArgs(2))
                '                  Eliminar_dia(empresa, dia)
                Dim Frm1 As New frmeliminar

                Frm1.ShowDialog()
                Me.Close()
            End If

            'Agregar_log(Variables.Xusuario, 6, "Inicio de Programa sin Parametros del Ejecutable", 1)
        End If

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        '        Dim dlg As New frmconfiguracion
        '
        '        dlg.ShowDialog()

    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim Cnn As New ADODB.Connection
        Dim SQl1 As String = ""
        Dim Rsta1 As New ADODB.Recordset
        Dim SQl2 As String = ""
        Dim Rsta2 As New ADODB.Recordset
        Dim Vector1(10) As String
        Dim Linea As String = ""
        Dim Valida As Boolean = False
        Dim Validabd(2)
        Dim mensaje As String = ""
        Dim CLinea As Integer = 0
        Dim XTipo1 As String = ""
        Dim XCodigo_log As Integer = 0
        Me.Cursor = Cursors.WaitCursor
        'Try
        'Abrir Archivo de Texto
        FileOpen(1, TextBox1.Text, OpenMode.Input, OpenAccess.Read)
        While Not EOF(1)

            CLinea = CLinea + 1
            Linea = LineInput(1)
            If Linea <> "" Then
                Vector1 = Split(Linea, ";")

                If Val(Vector1(0)) = 0 Then
                    'validar encabezado
                    Valida = Validar_Linea(Linea, 3)
                    If Valida = True Then
                        mensaje = "Error en Estructura de Archivo en Encabezado en Linea= " & CLinea
                        XCodigo_log = 10
                        Xempresa = ""
                    End If
                    Validabd = Valida_BD(Vector1(1), Vector1(2))
                    If Val(Validabd(0)) = 0 Then
                        mensaje = " Error No Existe Base de Datos en Servidor SQL, Favor Revisar"
                        XCodigo_log = 11
                    Else
                        XbdP = Validabd(1)
                        Xidempresa = Int(Validabd(0))
                        Abrir_ini(XbdP)
                        '                    Else
                        '                       XbdP = buscar_bd(Vector1(1), Vector1(2))
                        '                      CnnString = ("Provider=SQLOLEDB;Data Source=" & XServidorP & ";user id=" & XUsuarioP & "; password=" & XPasswordP & "; Initial Catalog=" & Vector1(1))
                    End If
                Else
                    If Val(Vector1(0)) = 1 Then
                        'validar encabezado de factura
                        Valida = Validar_Linea(Linea, 16)
                        If Valida = False Then
                            If Int(Vector1(7)) = 1 Then
                                Crear_Cliente(Vector1(5), Vector1(4), Vector1(5), Vector1(6))
                            End If
                            '   Valida = Validar_Cliente(Vector1(3))
                            If Valida = False Then
                                Valida = Validar_Vendedor(Vector1(8))
                                If Valida = False Then

                                Else
                                    Crear_Vendedor(Vector1(8), Vector1(9), Vector1(10))
                                    Valida = False
                                    '    mensaje = "Error de Codigo de Vendedor que No Existe: " & Vector1(8) & " en Linea=" & CLinea
                                    '   XCodigo_log = 12
                                End If
                            Else
                                mensaje = "Error de Codigo de Cliente que No Existe: " & Vector1(3) & " en Linea=" & CLinea
                                XCodigo_log = 13
                            End If
                        Else
                            mensaje = "Error en Estructura de Archivo en Factura en la Linea= " & CLinea
                            XCodigo_log = 14

                        End If
                    Else
                        If Val(Vector1(0)) = 2 Then
                            'validar reglones de pago
                            Valida = Validar_Linea(Linea, 7)
                            If Valida = True Then
                                mensaje = "Error en Estructura de Archivo en Cobro en la Linea= " & CLinea
                                XCodigo_log = 15
                            End If
                        Else
                            If Val(Vector1(0)) = 3 Or Val(Vector1(0)) = 4 Then
                                'validar reglones de pago
                                Valida = Validar_Linea(Linea, 8)
                                XTipo1 = IIf(Val(Vector1(0)) = 3, "Credito", "Debito")
                                If Valida = True Then
                                    mensaje = "Error en Estructura de Archivo en Nota de " & XTipo1 & " en la Linea= " & CLinea
                                    XCodigo_log = 16
                                End If
                            End If
                        End If
                    End If
                End If
            End If
            If Valida = True Then
                FileClose(1)
                Agregar_log(Variables.Xusuario, XCodigo_log, mensaje, Xidempresa)
                '        MsgBox("Error: " & mensaje, MsgBoxStyle.Critical, "Error en Archivo")
                Me.Cursor = Cursors.Default
                Exit Sub
            Else
                Valida = False
                '  Exit Sub
            End If
        End While
        CLinea = 0
        Dim Co_Empresa As String = ""
        Dim Fec_Proceso As String = ""
        Dim Xnro_Factura As Integer
        Dim XCo_cli As String = ""
        Dim XFecha As String = ""
        Dim XCo_ven As String = ""
        Dim XCobro As Integer
        Dim Xreng As Integer = 0
        Dim XReng_tip As Integer
        Dim XMov_caj As Integer = 0
        Dim XMov_Ban As Integer = 0
        Dim XMonto_Fac As String
        Dim XMonto_iva As String
        Dim VF As Boolean = False
        Seek(1, 1)

        While Not EOF(1)
            CLinea = CLinea + 1
            Linea = LineInput(1)
            If Linea <> "" Then
                Vector1 = Split(Linea, ";")
                Select Case Val(Vector1(0))
                    Case Is = 0
                        Co_Empresa = Xempresa
                        Fec_Proceso = Vector1(3)
                    Case Is = 1
                        VF = Valida_factura(Vector1(1))
                        If VF = False Then
                            XCo_cli = IIf(Vector1(7) = 0, XClienteO, Vector1(3))
                            Encabezado_Factura(Vector1(1), Vector1(2), XCo_cli, Vector1(4), Vector1(5), Vector1(6), Vector1(8), _
                                               Vector1(11), Vector1(12), Vector1(13), Vector1(14), Vector1(15), Vector1(16))
                            Xnro_Factura = Vector1(1)
                            XFecha = Vector1(2)
                            XCo_ven = Vector1(8)
                            XMonto_Fac = Vector1(14)
                            XMonto_iva = Vector1(13)
                            XCobro = 0
                            Xreng = 0
                        End If
                    Case Is = 2
                        Xreng = Xreng + 1
                        If XCobro = 0 Then
                            XCobro = Cobros(Xnro_Factura, XCo_cli, XCo_ven, XFecha, Vector1(5), XMonto_Fac, XMonto_iva)
                            Agregar_log(XUsuarioP, 21, "Creado cobro: " & XCobro, Xidempresa)
                            If XCobro > 0 Then
                                XReng_tip = Ingreso_Tips(XCobro, Xreng, Vector1(2), Vector1(4), Vector1(5), Vector1(7), Vector1(6), Vector1(3), Xnro_Factura)
                                Agregar_log(XUsuarioP, 26, "Ingresado tipo de cobro: " & XCobro, Xidempresa)
                            End If
                        Else
                            XReng_tip = Ingreso_Tips(XCobro, Xreng, Vector1(2), Vector1(4), Vector1(5), Vector1(7), Vector1(6), Vector1(3), Xnro_Factura)
                            Agregar_log(XUsuarioP, 26, "Ingresado tipo de cobro: " & XCobro, Xidempresa)
                        End If
                    Case Is = 3
                        Dim xbcliente As Boolean = False
                        Dim Datos_Fact(6) As String
                        Dim Nro_NCredito As Integer

                        Datos_Fact = Buscar_Cliente_Factura(Vector1(8))
                        If Datos_Fact(1) <> "" Then
                            If Datos_Fact(0) <> Datos_Fact(4) Then
                                If Len(Datos_Fact(4)) > 0 Then
                                    XCo_cli = Crear_Cliente(Datos_Fact(4), Datos_Fact(3), Datos_Fact(4), Datos_Fact(5))
                                Else
                                    Agregar_log(XUsuarioP, 27, "Error Cedula Vacia en N/CR: ", Xidempresa)
                                End If
                            End If
                            Nro_NCredito = CNota_credito(Vector1(1), Datos_Fact(1), Vector1(2), XCo_cli, Datos_Fact(2), Vector1(3), Vector1(4), Vector1(5), Vector1(6), Vector1(7), Vector1(8), Datos_Fact(3), Datos_Fact(4), Datos_Fact(5))
                            '  Agregar_log(XUsuarioP, 22, "Creada Nota de Credto: " & Nro_NCredito, Xidempresa)
                        End If
                    Case Is = 4



                End Select
            End If
        End While
        FileClose(1)
        'Catch ex As Exception
        ' MessageBox.Show(ex.Message, "Error")
        ' End Try
        Me.Cursor = Cursors.Default
        Me.Close()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Dim Documento As Object

        ofp1.Filter = "Archivos de Texto|*.txt"
        ofp1.ShowDialog()

        Documento = ofp1.FileName

        TextBox1.Text = Documento


    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Me.Close()
    End Sub

    Public Sub New()

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub
End Class
