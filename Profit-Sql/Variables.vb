﻿'Imports System.Data.SQLite
Module Variables
    Public XServidorP As String
    Public XUsuarioP As String
    Public XPasswordP As String
    Public XServidorICG As String
    Public XUsuarioICG As String
    Public XPasswordICG As String
    Public Xbdp1 As String
    Public CnnString As String
    Public CnnStringICG As String
    Dim f_HeightRatio As Single = New Single
    Dim f_WidthRatio As Single = New Single
    Public Sub ResizeForm(ByVal ObjForm As Form, ByVal DesignerWidth As Integer, ByVal DesignerHeight As Integer)
        Dim i_StandardHeight As Integer = DesignerHeight
        Dim i_StandardWidth As Integer = DesignerWidth
        Dim i_PresentHeight As Integer = Screen.PrimaryScreen.Bounds.Height
        Dim i_PresentWidth As Integer = Screen.PrimaryScreen.Bounds.Width
        f_HeightRatio = (CSng(i_PresentHeight) / CSng(i_StandardHeight))
        f_WidthRatio = (CSng(i_PresentWidth) / CSng(i_StandardWidth))
        ObjForm.AutoScaleMode = AutoScaleMode.None
        ObjForm.Scale(New SizeF(f_WidthRatio, f_HeightRatio))
        For Each c As Control In ObjForm.Controls
            If c.HasChildren Then
                ResizeControlStore(c)
            Else
                c.Font = New Font(c.Font.FontFamily, c.Font.Size * f_HeightRatio, c.Font.Style, c.Font.Unit, (CByte(0)))
            End If
        Next
        ObjForm.Font = New Font(ObjForm.Font.FontFamily, ObjForm.Font.Size * f_HeightRatio, ObjForm.Font.Style, ObjForm.Font.Unit, (CByte(0)))
    End Sub
    Private Sub ResizeControlStore(ByVal objCtl As Control)
        If objCtl.HasChildren Then
            For Each cChildren As Control In objCtl.Controls
                If cChildren.HasChildren Then
                    ResizeControlStore(cChildren)
                Else
                    cChildren.Font = New Font(cChildren.Font.FontFamily, cChildren.Font.Size * f_HeightRatio, cChildren.Font.Style, cChildren.Font.Unit, (CByte(0)))
                End If
            Next
            objCtl.Font = New Font(objCtl.Font.FontFamily, objCtl.Font.Size * f_HeightRatio, objCtl.Font.Style, objCtl.Font.Unit, (CByte(0)))
        Else
            objCtl.Font = New Font(objCtl.Font.FontFamily, objCtl.Font.Size * f_HeightRatio, objCtl.Font.Style, objCtl.Font.Unit, (CByte(0)))
        End If
    End Sub
    Public Sub Abrir_ini()
        Dim a1 As Integer
        Dim Linea As String = ""
        Dim Xbdp As String = ""
        '        Dim Xbdp1 As String = ""

        FileOpen(1, My.Application.Info.DirectoryPath & "\config1.ini", OpenMode.Input)
        Linea = LineInput(1)
        a1 = InStr(1, Linea, ":") + 1
        XServidorP = IIf(a1 > 0, Trim(Mid(Linea, a1, Len(Linea))), "")
        Linea = LineInput(1)
        a1 = InStr(1, Linea, ":") + 1
        XUsuarioP = IIf(a1 > 0, Trim(Mid(Linea, a1, Len(Linea))), "")
        Linea = LineInput(1)
        a1 = InStr(1, Linea, ":") + 1
        XPasswordP = IIf(a1 > 0, Trim(Mid(Linea, a1, Len(Linea))), "")
        Linea = LineInput(1)
        a1 = InStr(1, Linea, ":") + 1
        Xbdp1 = IIf(a1 > 0, Trim(Mid(Linea, a1, Len(Linea))), "")
        Xbdp = "master"
        Linea = LineInput(1)
        a1 = InStr(1, Linea, ":") + 1
        XServidorICG = IIf(a1 > 0, Trim(Mid(Linea, a1, Len(Linea))), "")
        Linea = LineInput(1)
        a1 = InStr(1, Linea, ":") + 1
        XUsuarioICG = IIf(a1 > 0, Trim(Mid(Linea, a1, Len(Linea))), "")
        Linea = LineInput(1)
        a1 = InStr(1, Linea, ":") + 1
        XPasswordICG = IIf(a1 > 0, Trim(Mid(Linea, a1, Len(Linea))), "")
        FileClose(1)
        CnnString = ("Provider=SQLOLEDB;Data Source=" & XServidorP & ";user id=" & XUsuarioP & "; password=" & XPasswordP & "; Initial Catalog=" & Xbdp)
        CnnStringICG = ("Provider=SQLOLEDB;Data Source=" & XServidorICG & ";user id=" & XUsuarioICG & "; password=" & XPasswordICG & "; Initial Catalog=" & Xbdp)
    End Sub
    Public Function Buscar_BD(ByVal rif As String, ByVal sucursal As String)
        Dim Cnn As New ADODB.Connection
        Dim SQl1 As String = ""
        Dim Rsta1 As New ADODB.Recordset
        Dim SQl2 As String = ""
        Dim Rsta2 As New ADODB.Recordset
        Dim CEmpresa As String = ""
        Dim Validar1 As Integer = 0
        CnnString = ("Provider=SQLOLEDB;Data Source=" & XServidorP & ";user id=" & XUsuarioP & "; password=" & XPasswordP & "; Initial Catalog=" & Xbdp1)

        Cnn.Open(CnnString)
        SQl1 = "select coalesce(id_empresa,0) as empresa from empresa where rif='" & rif & "' and sucursal='" & sucursal & "'"
        Rsta1.Open(SQl1, Cnn, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockReadOnly)
        If Not Rsta1.EOF Then
            Validar1 = Rsta1.Fields("empresa").Value
        End If
        Rsta1.Close()
        Cnn.Close()

        Return Validar1
    End Function
    Public Function Actualiza_BD(ByVal id As Integer, ByVal Rif As String, ByVal Sucursal As Integer)
        Dim Cnn As New ADODB.Connection
        Dim SQl1 As String = ""
        Dim Rsta1 As New ADODB.Recordset
        Dim XConcepto As String = ""

        CnnString = ("Provider=SQLOLEDB;Data Source=" & XServidorP & ";user id=" & XUsuarioP & "; password=" & XPasswordP & "; Initial Catalog=" & Xbdp1)

        Cnn.Open(CnnString)
        SQl1 = "update empresa set rif='" & Rif & "', sucursal=" & Sucursal & " where id_empresa=" & id
        Rsta1.Open(SQl1, Cnn, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockReadOnly)
        If id > 0 Then
            XConcepto = "Se Actualizo la  Empresa id:" & id & " Rif: " & Rif & " Sucursal: " & Sucursal
            Agregar_log(XUsuarioP, 2, XConcepto, id)
        End If

        Cnn.Close()

        Return True
    End Function

    Public Function Guardar_BD(ByVal rif As String, ByVal sucursal As String, ByVal Based As String)
        Dim Cnn As New ADODB.Connection
        Dim SQl1 As String = ""
        Dim Rsta1 As New ADODB.Recordset
        Dim XConcepto As String = ""
        'CnnString = ("Provider=SQLOLEDB;Data Source=" & XServidorP & ";user id=" & XUsuarioP & "; password=" & XPasswordP & "; Initial Catalog=" & Xbdp1)

        Cnn.Open(CnnString)
        SQl1 = "insert into empresa(rif,sucursal,empresap) values('" & rif & "'," & _
                                 sucursal & ",'" & Based & "'" & ")"
        Rsta1.Open(SQl1, Cnn, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockReadOnly)
        SQl1 = "select max(id_empresa) as empresa from empresa "
        Rsta1.Open(SQl1, Cnn, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockReadOnly)

        If Rsta1.Fields("empresa").Value > 0 Then
            XConcepto = "Se Agrego Empresa id:" & Rsta1.Fields("empresa").Value & " Rif: " & rif & " Sucursal: " & sucursal
            Agregar_log(XUsuarioP, 1, XConcepto, Rsta1.Fields("empresa").Value)
        End If

        Cnn.Close()

        Return True
    End Function
    Public Function Agregar_log(ByVal user As String, ByVal codigo As Integer, ByVal op_string As String, ByVal empresa As String)
        Dim Cnn As New ADODB.Connection
        Dim SQl1 As String = ""
        Dim Rsta1 As New ADODB.Recordset
        Abrir_ini()
        CnnString = ("Provider=SQLOLEDB;Data Source=" & XServidorP & ";user id=" & XUsuarioP & "; password=" & XPasswordP & "; Initial Catalog=" & Xbdp1)
        Cnn.Open(CnnString)
        SQl1 = "insert into bitx02(op_user,op_fecha_hora,op_codigo,op_string,id_empresa) values('" & XUsuarioP & "',convert(datetime,'" & Mid(Now.Date.ToString, 1, 10) & " " & _
                                         Now.Hour & ":" & Now.Minute & "',103)," & codigo & ",'" & op_string & "'," & empresa & ")"
        Rsta1.Open(SQl1, Cnn, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockReadOnly)

        Cnn.Close()

        Return True
    End Function


    Public Function buscar_BdIcg(ByVal rif As String, ByVal sucursal As Integer)
        Dim Nombre_ICG As String = ""
        Dim Cnn As New ADODB.Connection
        Dim Sql1 As String
        Dim Rsta1 As New ADODB.Recordset
        Dim XXbd As String = ""
        Dim Cnn1 As New ADODB.Connection
        Dim Sql2 As String
        Dim Rsta2 As New ADODB.Recordset
        Dim Sql3 As String
        Dim Rsta3 As New ADODB.Recordset

        Cnn.Open(Variables.CnnStringICG)
        Sql1 = "select * from sys.databases"
        Rsta1.Open(Sql1, Cnn, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockReadOnly)
        While Not Rsta1.EOF

            XXbd = Trim(Rsta1.Fields("name").Value)
            CnnStringICG = ("Provider=SQLOLEDB;Data Source=" & XServidorICG & ";user id=" & XUsuarioICG & "; password=" & XPasswordICG & "; Initial Catalog=" & XXbd)
            Cnn1.Open(CnnStringICG)

            Sql2 = "select count(*) as tipo from INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE = 'BASE TABLE' and TABLE_NAME = 'SERIESCAMPOSLIBRES'"
            Rsta2.Open(Sql2, Cnn1, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockReadOnly)

            If Rsta2.Fields("tipo").Value > 0 Then
                Sql3 = " SELECT RIF, SUCURSAL FROM SERIESCAMPOSLIBRES " & _
                        " WHERE rif='" & rif & "' and sucursal=" & sucursal & " GROUP BY RIF, SUCURSAL"
                Rsta3.Open(Sql3, Cnn1, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockReadOnly)
                If Not Rsta3.EOF Then
                    Nombre_ICG = XXbd
                    Exit While
                Else
                    Nombre_ICG = ""
                End If
                Rsta3.Close()
            End If
            Rsta2.Close()
            Cnn1.Close()
            Rsta1.MoveNext()
        End While
        Rsta1.Close()
        Cnn.Close()

        Return Nombre_ICG
    End Function
End Module
